# Seref C++ 库

Seref 是一个用于 C++ 的轻量级 Header-only 序列化和反序列化库。它提供了简洁的宏和概念，使得在用户定义的类型上执行序列化和反序列化变得更加容易。此外，Seref 还提供了一些简单的反射功能，帮助用户在运行时访问和操作对象的结构。


## 安装

```cpp
#include "seref.hpp"
```

Seref 是一个 Header-only 库，你只需要将 seref.hpp 包含到你的项目中即可开始使用。

