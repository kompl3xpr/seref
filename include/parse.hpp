#pragma once

#include <concepts>
#include <sstream>
#include <string>

#include "debug.hpp"
#include "macros.hpp"

namespace seref {
template <typename T>
concept InputStream = requires(T t, int n) {
    { t >> n };
};

template <InputStream I>
class ParseDesarializer {
    NON_COPYABLE(ParseDesarializer)
    I& _inputStream;

   public:
    ParseDesarializer(I& inputStream) : _inputStream{inputStream} {}

    auto& beginClass(const std::string& name) { return *this; }
    auto& endClass() { return *this; }

    template <typename T>
    auto& addField(const std::string& name, T& value) {
        return *this;
    }

    template <typename T>
    auto& addEnum(const std::string& name, T& value) {
        std::string s;
        _inputStream >> s;
        value.setFromStr(s);
        return *this;
    }

    auto& add(bool& value) {
        return *this;
    }

    auto& add(std::string& value) {
        _inputStream >> value;
        return *this;
    }

    auto& add(char& value) {
        _inputStream >> value;
        return *this;
    }

    auto& add(Numeric auto& value) {
        _inputStream >> value;
        return *this;
    }

    template <Byte T>
    auto& add(T& value) {
        int16_t num;
        _inputStream >> num;
        value = static_cast<T>(num);
        return *this;
    }

    auto& add(Deserializable auto& value) {
        value.deserialize(*this);
        return *this;
    }

    template <typename T>
    auto& add(T& value) {
        return *this;
    }
};

template <typename T>
void parseInto(const std::string& input, T& value) {
    std::istringstream is{input};
    ParseDesarializer<std::istringstream> parser{is};
    parser.add(value);
}

}  // namespace seref