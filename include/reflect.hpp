#pragma once

#include <string>
#include <concepts>

namespace seref {
class Reflect {
   public:
    virtual void setFromStr(const std::string& field,
                            const std::string& value) = 0;

    virtual std::string getStr(const std::string& field) const = 0;

    virtual ~Reflect() = default;
};

template <typename T>
requires (!std::is_base_of_v<Reflect, T>)
Reflect* reflect_cast(T*) {
    return nullptr;
}

template <typename T>
requires std::is_base_of_v<Reflect, T>
Reflect* reflect_cast(T* ptr) {
    return dynamic_cast<Reflect*>(ptr);
}

template <typename T>
requires (!std::is_base_of_v<Reflect, T>)
const Reflect* reflect_cast(const T*) {
    return nullptr;
}

template <typename T>
requires std::is_base_of_v<Reflect, T>
const Reflect* reflect_cast(const T* ptr) {
    return dynamic_cast<const Reflect*>(ptr);
}

}  // namespace seref