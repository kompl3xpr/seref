#pragma once

#include <array>
#include <concepts>
#include <format>
#include <map>
#include <unordered_map>
#include <variant>
#include <vector>

#include "macros.hpp"
#include "serde.hpp"

namespace seref {

template <typename T>
concept OutputStream = requires(T t) {
    { t << "" } -> std::same_as<T&>;
};

template <OutputStream O>
class DebugSerializer {
    NON_COPYABLE(DebugSerializer)

    O& _outputStream;
    size_t _item_count{0};
    size_t _indent;
    bool _comma;
    bool _newline;

   private:
    void outputIndent() {
        for (size_t i = 0; i < _indent; i++) {
            _outputStream << "    ";
        }
    }

    void outputNewline() {
        _outputStream << (_comma ? "," : "");
        _outputStream << (_newline ? "\n" : "");
    }

   public:
    DebugSerializer(O& outputStream, size_t indent = 0, bool comma = false,
                    bool newline = false)
        : _outputStream{outputStream},
          _indent{indent},
          _comma{comma},
          _newline{newline} {}

    DebugSerializer& beginClass(const std::string& name) {
        _outputStream << name << "(";
        _indent += 1;
        return *this;
    }

    template <typename T>
    DebugSerializer& addField(const std::string& name, const T& value) {
        if (_item_count == 0) {
            _outputStream << "\n";
        }
        this->outputIndent();
        _outputStream << name << ": ";
        DebugSerializer s{_outputStream, _indent, true, true};
        s.add(value);
        _item_count += 1;
        return *this;
    }

    DebugSerializer& endClass() {
        _indent -= 1;
        if (_item_count > 0) {
            outputIndent();
        }
        _outputStream << ")";
        outputNewline();
        _item_count = 0;
        return *this;
    }

    template <typename T>
    DebugSerializer& addEnum(const std::string& type, const T& e) {
        _outputStream << type << "::" << e.toStr();
        outputNewline();
        return *this;
    }

    DebugSerializer& beginList() {
        _outputStream << "[";
        _indent += 1;
        return *this;
    }

    template <typename T>
    DebugSerializer& addElement(const T& value) {
        if (_item_count == 0) {
            _outputStream << "\n";
        }
        this->outputIndent();
        DebugSerializer s{_outputStream, _indent, true, true};
        s.add(value);
        _item_count += 1;
        return *this;
    }

    DebugSerializer& endList() {
        _indent -= 1;
        if (_item_count > 0) {
            outputIndent();
        }
        _outputStream << "]";
        outputNewline();
        _item_count = 0;
        return *this;
    }

    DebugSerializer& beginMap() {
        _outputStream << "{";
        _indent += 1;
        return *this;
    }

    template <typename K, typename V>
    DebugSerializer& addEntry(const K& key, const V& value) {
        if (_item_count == 0) {
            _outputStream << "\n";
        }
        this->outputIndent();
        DebugSerializer s1{_outputStream, _indent};
        s1.add(key);
        _outputStream << ": ";
        DebugSerializer s2{_outputStream, _indent, true, true};
        s2.add(value);
        _item_count += 1;
        return *this;
    }

    DebugSerializer& endMap() {
        _indent -= 1;
        if (_item_count > 0) {
            outputIndent();
        }
        _outputStream << "}";
        outputNewline();
        _item_count = 0;
        return *this;
    }

    DebugSerializer& add(const std::string& value) {
        _outputStream << "\"" << value << "\"";
        outputNewline();
        return *this;
    }

    DebugSerializer& add(const char& value) {
        _outputStream << "'" << value << "'";
        outputNewline();
        return *this;
    }

    DebugSerializer& add(const bool& value) {
        const char* s = value ? "true" : "false";
        _outputStream << s;
        outputNewline();
        return *this;
    }

    template <typename K, typename V>
    DebugSerializer& add(const std::map<K, V>& value) {
        this->beginMap();
        for (const auto& [k, v] : value) {
            this->addEntry(k, v);
        }
        this->endMap();
        return *this;
    }

    template <typename T, size_t N>
    DebugSerializer& add(const std::array<T, N>& value) {
        this->beginList();
        for (const auto& elem : value) {
            this->addElement(elem);
        }
        this->endList();
        return *this;
    }

    template <typename T>
    DebugSerializer& add(const std::vector<T>& value) {
        this->beginList();
        for (const auto& elem : value) {
            this->addElement(elem);
        }
        this->endList();
        return *this;
    }

    template <typename T>
    DebugSerializer& add(const bool& value) {
        const char* s = value ? "true" : "false";
        _outputStream << s;
        outputNewline();
        return *this;
    }

    template <Byte T>
    DebugSerializer& add(const T& value) {
        _outputStream << static_cast<int16_t>(value);
        outputNewline();
        return *this;
    }

    template <Numeric T>
    DebugSerializer& add(const T& value) {
        _outputStream << value;
        outputNewline();
        return *this;
    }

    template <typename T>
        requires std::is_pointer_v<T>
    DebugSerializer& add(const T& value) {
        _outputStream << value;
        outputNewline();
        return *this;
    }

    template <typename T>
        requires std::is_reference_v<T>
    DebugSerializer& add(const T& value) {
        _outputStream << &value;
        outputNewline();
        return *this;
    }

    DebugSerializer& add(Serializable auto& value) {
        value.serialize(*this);
        return *this;
    }

    template <typename T>
    DebugSerializer& add(T& value) {
        _outputStream << std::format("#valueoftype_{:016x}",
                                     typeid(T).hash_code());
        outputNewline();
        return *this;
    }
};

template <typename T>
class DebugFmt {
    NON_COPYABLE(DebugFmt)
    const T& _value;

   public:
    DebugFmt(const T& value) : _value{value} {}

    friend std::ostream& operator<<(std::ostream& os, const DebugFmt& d) {
        DebugSerializer s{os};
        s.add(d._value);
        return os;
    }
};

}  // namespace seref