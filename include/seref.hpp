#pragma once

#include <array>
#include <span>
#include <sstream>
#include <typeinfo>
#include <unordered_map>

#include "macros.hpp"
#include "debug.hpp"
#include "macro_apply.hpp"
#include "parse.hpp"
#include "reflect.hpp"
#include "serde.hpp"

#define FIELD(FieldType, FieldName) FieldType, FieldName
#define _SEREF_EXPAND(...) __VA_ARGS__

struct FieldInfo {
    size_t offset;
    const std::type_info& typeInfo;


    FieldInfo(size_t offset, const std::type_info& typeInfo)
        : offset{offset}, typeInfo{typeInfo} {}
};

#define _SEREF_GET_FIELD_INFO(FieldType, FieldName)                         \
    {#FieldName,                                                              \
     FieldInfo{[] {                                                           \
                   FieldType __SEREF_This_Class::*ptr =                     \
                       &__SEREF_This_Class::FieldName;                      \
                   return reinterpret_cast<size_t>(                           \
                       &(static_cast<__SEREF_This_Class*>(nullptr)->*ptr)); \
               }(),                                                           \
               typeid(FieldType)}},

#define _SEREF_SERDE_ADD_FIELD(FieldType, FieldName) \
    .addField(#FieldName, this->FieldName)

#define _SEREF_COPY_FIELD(FieldType, FieldName)                            \
    FieldType* other_##FieldName = _seref_other.get<FieldType>(#FieldName); \
    if (other_##FieldName != nullptr) {                                      \
        *other_##FieldName = this->FieldName;                                \
    }

#define _SEREF_GET_FIELD_NAME(FieldType, FieldName) #FieldName,

#define _SEREF_GET_FIELD_VALUE_STR(FieldType, FieldName) \
    if (_seref_field_name == #FieldName) {                \
        ss << seref::DebugFmt(this->FieldName);           \
        return ss.str();                                   \
    }

#define _SEREF_SET_FIELD_VALUE_STR(FieldType, FieldName)       \
    if (_seref_field_name == #FieldName) {                      \
        seref::parseInto(_seref_field_value, this->FieldName); \
        return;                                                  \
    }

#define _SEREF_GET_OBJECT_FIELD(FieldType, FieldName) \
    if (_seref_field_name == #FieldName) {             \
        return seref::reflect_cast(&this->FieldName);  \
    }

#define SEREF_CLASS(ClassType, FieldCount, ...)                              \
                                                                               \
   private:                                                                    \
    using __SEREF_This_Class = ClassType;                                    \
    inline static const std::unordered_map<std::string, FieldInfo>             \
        __SEREF_FIELD_INFOS{_SEREF_EXPAND(                                 \
            M_APPLY2_##FieldCount(_SEREF_GET_FIELD_INFO, __VA_ARGS__))};     \
                                                                               \
    inline static const std::array<const char*, FieldCount>                    \
        __SEREF_FIELD_NAMES{_SEREF_EXPAND(                                 \
            M_APPLY2_##FieldCount(_SEREF_GET_FIELD_NAME, __VA_ARGS__))};     \
                                                                               \
   public:                                                                     \
    template <typename T>                                                      \
    constexpr T* get(const std::string& _seref_field_name) {                  \
        auto iter = __SEREF_FIELD_INFOS.find(_seref_field_name);            \
        if (iter == __SEREF_FIELD_INFOS.cend() ||                            \
            iter->second.typeInfo.hash_code() != typeid(T).hash_code()) {      \
            return nullptr;                                                    \
        }                                                                      \
        uint8_t* ptr = reinterpret_cast<uint8_t*>(this) + iter->second.offset; \
        return reinterpret_cast<T*>(ptr);                                      \
    }                                                                          \
                                                                               \
    template <typename T>                                                      \
    constexpr const T* get(const std::string& _seref_field_name) const {      \
        const auto iter = __SEREF_FIELD_INFOS.find(_seref_field_name);      \
        if (iter == __SEREF_FIELD_INFOS.cend() ||                            \
            iter->second.typeInfo.hash_code() != typeid(T).hash_code()) {      \
            return nullptr;                                                    \
        }                                                                      \
        const uint8_t* ptr =                                                   \
            reinterpret_cast<const uint8_t*>(this) + iter->second.offset;      \
        return reinterpret_cast<const T*>(ptr);                                \
    }                                                                          \
                                                                               \
    std::string getStr(const std::string& _seref_field_name) const {          \
        std::stringstream ss;                                                  \
        _SEREF_EXPAND(                                                       \
            M_APPLY2_##FieldCount(_SEREF_GET_FIELD_VALUE_STR, __VA_ARGS__))  \
        return "";                                                             \
    }                                                                          \
                                                                               \
    constexpr void setFromStr(const std::string& _seref_field_name,           \
                              const std::string& _seref_field_value) {        \
        _SEREF_EXPAND(                                                       \
            M_APPLY2_##FieldCount(_SEREF_SET_FIELD_VALUE_STR, __VA_ARGS__))  \
    }                                                                          \
                                                                               \
    constexpr seref::Reflect* getObject(const std::string& _seref_field_name) {      \
        _SEREF_EXPAND(                                                       \
            M_APPLY2_##FieldCount(_SEREF_GET_OBJECT_FIELD, __VA_ARGS__))     \
        return nullptr;                                                        \
    }                                                                          \
                                                                               \
    constexpr const seref::Reflect* getObject(const std::string& _seref_field_name)  \
        const {                                                                \
        _SEREF_EXPAND(                                                       \
            M_APPLY2_##FieldCount(_SEREF_GET_OBJECT_FIELD, __VA_ARGS__))     \
        return nullptr;                                                        \
    }                                                                          \
                                                                               \
    static const std::type_info* getTypeOf(                                    \
        const std::string& _seref_field_name) {                               \
        const auto iter = __SEREF_FIELD_INFOS.find(_seref_field_name);      \
        if (iter == __SEREF_FIELD_INFOS.cend()) {                            \
            return nullptr;                                                    \
        }                                                                      \
        return &iter->second.typeInfo;                                         \
    }                                                                          \
                                                                               \
    constexpr static size_t getFieldCount() { return FieldCount; }             \
                                                                               \
    constexpr static const auto getFieldNames() {                              \
        return std::span{__SEREF_FIELD_NAMES};                               \
    }                                                                          \
                                                                               \
    template <typename T>                                                      \
    constexpr void copyInto(T& _seref_other) const {                          \
        _SEREF_EXPAND(                                                       \
            M_APPLY2_##FieldCount(_SEREF_COPY_FIELD, __VA_ARGS__))           \
    }                                                                          \
                                                                               \
    constexpr void serialize(seref::Serializer auto& serializer) const {      \
        serializer                                                             \
            .beginClass(#ClassType) _SEREF_EXPAND(                           \
                M_APPLY2_##FieldCount(_SEREF_SERDE_ADD_FIELD, __VA_ARGS__))  \
            .endClass();                                                       \
    }                                                                          \
                                                                               \
    constexpr void deserialize(seref::Deserializer auto& deserializer) {      \
        deserializer                                                           \
            .beginClass(#ClassType) _SEREF_EXPAND(                           \
                M_APPLY2_##FieldCount(_SEREF_SERDE_ADD_FIELD, __VA_ARGS__))  \
            .endClass();                                                       \
    }                                                                          \
                                                                               \
   private:

#define _SEREF_ENUM_ITEM_TO_STR(ItemName) \
    case (__SEREF_This_Class::ItemName):  \
        return #ItemName;

#define _SEREF_ENUM_STR_TO_ITEM(ItemName)                \
    if (s == #ItemName) {                                  \
        this->getValue() = __SEREF_This_Class::ItemName; \
        return true;                                       \
    }

#define _SEREF_ENUM_GET_ITEM_STR(ItemName) #ItemName,

#define SEREF_ENUM(ClassType, EnumMember, ItemCount, ...)                   \
   private:                                                                   \
    using __SEREF_This_Class = ClassType;                                   \
    inline static const std::array<const char*, ItemCount> __SEREF_ITEMS{   \
        _SEREF_EXPAND(                                                      \
            M_APPLY1_##ItemCount(_SEREF_ENUM_GET_ITEM_STR, __VA_ARGS__))};  \
                                                                              \
   public:                                                                    \
    constexpr auto& getValue() { return this->EnumMember; }                   \
    constexpr const auto& getValue() const { return this->EnumMember; }       \
    constexpr const char* toStr() const {                                     \
        switch (this->EnumMember) {                                           \
            _SEREF_EXPAND(                                                  \
                M_APPLY1_##ItemCount(_SEREF_ENUM_ITEM_TO_STR, __VA_ARGS__)) \
            default:                                                          \
                return "";                                                    \
        }                                                                     \
    }                                                                         \
    constexpr bool setFromStr(const std::string& s) {                         \
        _SEREF_EXPAND(                                                      \
            M_APPLY1_##ItemCount(_SEREF_ENUM_STR_TO_ITEM, __VA_ARGS__))     \
        return false;                                                         \
    }                                                                         \
    constexpr static size_t getItemCount() { return ItemCount; }              \
    constexpr static const auto& getItems() {                                 \
        return std::span{__SEREF_ITEMS};                                    \
    }                                                                         \
    constexpr void serialize(seref::Serializer auto& serializer) const {     \
        serializer.addEnum(#ClassType, *this);                                \
    }                                                                         \
                                                                              \
    constexpr void deserialize(seref::Deserializer auto& deserializer) {     \
        deserializer.addEnum(#ClassType, *this);                              \
    }                                                                         \
                                                                              \
   private:
