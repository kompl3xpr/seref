#pragma once

#include <concepts>
#include <string>


namespace seref {
class SerializableTestEnum;

template <typename T>
concept Serializer =
    requires(T t, const std::string& name, const SerializableTestEnum& e) {
        { t.beginClass(name) } -> std::same_as<T&>;
        { t.addField(name, 1) } -> std::same_as<T&>;
        { t.endClass() } -> std::same_as<T&>;

        { t.addEnum(name, e) } -> std::same_as<T&>;

        { t.add(1) } -> std::same_as<T&>;
        { t.add(0.5) } -> std::same_as<T&>;
        { t.add(true) } -> std::same_as<T&>;
        { t.add("") } -> std::same_as<T&>;
        { t.add('a') } -> std::same_as<T&>;

        { t.beginMap() } -> std::same_as<T&>;
        { t.addEntry("a", 1) } -> std::same_as<T&>;
        { t.endMap() } -> std::same_as<T&>;

        { t.beginList() } -> std::same_as<T&>;
        { t.addElement(1) } -> std::same_as<T&>;
        { t.endList() } -> std::same_as<T&>;
    };

template <typename T>
concept Serializable = requires(T t) {
    { t.serialize };
};

template <typename T>
concept Deserializer =
    requires(T t, const std::string& name, SerializableTestEnum& e, int& n) {
        { t.beginClass(name) } -> std::same_as<T&>;
        { t.addField(name, n) } -> std::same_as<T&>;
        { t.endClass() } -> std::same_as<T&>;

        { t.addEnum(name, e) } -> std::same_as<T&>;

        { t.add(n) } -> std::same_as<T&>;
    };

template <typename T>
concept Deserializable = requires(T t) {
    { t.deserialize };
};

class SerializableTestEnum {
   public:
    void serialize(Serializer auto& s) {}
    // inline const char* toStr() { return ""; }
    // inline void setFromStr(const std::string& s) {}
};

template <typename T>
concept Byte = std::is_same_v<T, int8_t> || std::is_same_v<T, uint8_t>;

template <typename T>
concept Numeric = std::is_arithmetic_v<T> && !std::is_same_v<T, bool> &&
                  !Byte<T> && !std::is_same_v<T, char>;

}  // namespace seref